<?php

class PrintBookHooks {

	/**
	 * Callback for SidebarBeforeOutput hook
	 *
	 * @param Skin $skin
	 * @param array &$sidebar
	 */
	public static function onSidebarBeforeOutput( Skin $skin, &$sidebar ) {
	
		$title = $skin->getTitle();
		$articleName = preg_replace("/(Speciální|Special):PrintBook\//", "", $title->getPrefixedText());
		if(!preg_match("/(Soubor|File|Diskuse|Talk|MediaWiki|Widget|Module|Modul|Forum|Fórum|Speciální|Special|Widget|Kategorie|Category|Šablona|Template|Procvičování|Training):/", $articleName)) {
			$sidebar[ 'printbook-menu' ] = [
				[
					'msg' => 'printbook-menu-print',
					'href' => SpecialPage::getTitleFor( 'PrintBook' )->getLocalURL() . "/" . urlencode(str_replace(' ', '_', $articleName))
				],
				[
					'text' => $skin->msg( 'printableversion' )->text(),
					'id' => 't-printbook-print',
					'href' => $title->getLocalURL( [ 'useskin' => 'printer' ] )
				]
			];
		}
		else {
			$sidebar[ 'printbook-menu' ] = [
				[
				'text' => $skin->msg( 'printableversion' )->text(),
				'id' => 't-printbook-print',
				'href' => $title->getLocalURL( [ 'useskin' => 'printer' ] )
				]
			];
		}
	}

	/* run after saving article - create new cache */
	public static function onEditPage_attemptSave_after( EditPage $editPage, Status $status, $details ) {
		global $wgPrintBookRenderServerPath;
		//file_put_contents("/usr/local/www/nginx/wiki/extensions/PrintBook/debug.txt", "aaaaaaaaaa");
		$pageId = $editPage->getTitle()->getArticleID();
		$wikiurl = rtrim( WebRequest::detectServer().dirname( $_SERVER['SCRIPT_NAME'] ), '\/' );
		$thisWikiDomain = preg_replace("/https:\/\//", "", $wikiurl);
		// Start conversion - fire and forget
		$command = "/usr/local/bin/curl \"$wgPrintBookRenderServerPath/request.php?id=$pageId";
		$command .= "&wikiurl=" . $thisWikiDomain . "\" > /dev/null &";
		shell_exec($command);
		//file_put_contents("/usr/local/www/nginx/wiki/extensions/PrintBook/debug.txt", $command, FILE_APPEND);
	}
}

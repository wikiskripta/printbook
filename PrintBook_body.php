<?php
/**
 * SpecialPage for PrintBook extension
 * Called /param=article_or_book_name, renders PDF
 * @ingroup Extensions
 */

class PrintBook extends SpecialPage {
    function __construct() {
        parent::__construct('PrintBook');
    }

    function execute($param) {

        global $wgPrintBookRenderServerPath;
        $this->setHeaders();
        $out = $this->getOutput();
        $request = $this->getRequest();
        $config = $this->getConfig();

        # URL of this wiki
        $wikiurl = rtrim( WebRequest::detectServer().dirname( $_SERVER['SCRIPT_NAME'] ), '\/' );

        if(empty($param)) {
            $out->addHTML($this->msg('printbook-desc')->plain());
            return true;
        }
        else {
            $pageName = trim($param);
            $pageId = false;
            $info = "<div id='pbInfo' class='mb-3'>" . preg_replace("/#NAME#/", "<strong>$pageName</strong>", $this->msg('printbook-inprogress')->plain()) . "<br>";
            $info .= $this->msg('printbook-info')->plain() . "</div>";
            $out->addHTML($info);
        }

        // check if $pageName is article or book
        $params = [
            "action" => "query",
            "format" => "json",
            "prop" => "categories",
            "titles" => $pageName
        ];
        $url = $wikiurl . "/api.php?" . http_build_query( $params );
        $ch = curl_init( $url );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        $output = curl_exec( $ch );
        curl_close( $ch );
        $result = json_decode( $output, true );
        $isBook = false;

        if(isset($result["query"]["pages"])) {
            foreach( $result["query"]["pages"] as $k => $v ) {
                if($k == '-1') {
                    $out->addHTML("<div class='alert alert-danger' role='alert' class='mt-3 mb-3'>" . $this->msg('printbook-error-nonexistent')->plain() . "</div>");
                    return true;
                }
                else {
                    $pageId = $k;
                    $ns = $v["ns"]; 
                }
                if(isset($v["categories"])) {
                    foreach( $v["categories"] as $cat ) {
                        if($cat["title"] == 'Kategorie:Knihy' || $cat["title"] == 'Category:Books') {
                            $isBook = true;
                            break;
                        }
                    }
                }
                # only allowed namespaces
                $allowedNS = false;
                foreach($config->get("PrintBookAllowedNamespaces") as $ans) {
                    if($ns == $ans[0]) { $allowedNS = true; break; }
                }
                if($allowedNS == false) return true;
            }
        }
        else return true;

        // only allowed namespaces
        $allowedNS = false;
        $a = Article::newFromId($pageId);
        $ns = $a->getTitle()->getNamespace();
        foreach($config->get("PrintBookAllowedNamespaces") as $ans) {
            if($ns == $ans[0]) { $allowedNS = true; break; }
        }
        if($allowedNS == false) {
            $out->addHTML("<div class='alert alert-danger' role='alert' class='mt-3 mb-3'>" . $this->msg('printbook-error-ns-not-allowed')->plain() . "</div>");
            $out->addHTML("<button id='pbBackBtn' class='btn btn-secondary d-none role='link' type='button' data-href='$pageName'>");
            return true;
        }

        $messages = "<div id='printBookError' class='alert alert-danger d-none' role='alert' class='mt-3 mb-3'>" . $this->msg('printbook-error')->plain() . "</div>";
        $messages .= "<div id='printBookSuccess' class='alert alert-success d-none' role='alert' class='mt-3 mb-3'>" . $this->msg('printbook-success')->plain() . "</div>";

        // check if cache is available
        $thisWikiDomain = preg_replace("/https:\/\//", "", $wikiurl);
        try {
            $res = file_get_contents("$wgPrintBookRenderServerPath/data/$thisWikiDomain/$pageId/progress.txt");
            $arr = explode('/', $res);
            $res = $arr[0];
        }
        catch(Exception $e) {
            $res = "missing";
        }

        if(!$isBook) {
            // ARTICLE
            if($res == 'finished') {
                // download article
                $downloadUrl = "$wgPrintBookRenderServerPath/data/$thisWikiDomain/$pageId/Article.pdf";
                header("location: $downloadUrl");
                return true;
            }
            else {
                $out->addHTML("<div id='printBookError' class='alert alert-danger' role='alert' class='mt-3 mb-3'>" . $this->msg('printbook-error')->plain() . "</div>");
                $out->addHTML("<button id='pbBackBtn' class='btn btn-secondary d-none role='link' type='button' data-href='$pageName'>");
                return true;
            }
        }
        else {
            // BOOK
            $tokenLength = 20;
            $bookID = bin2hex(random_bytes($tokenLength));
            $out->addHTML("<div id='pbConfig' class='d-none' data-renderpath='$wgPrintBookRenderServerPath' data-pageid='$pageId' data-bookid='$bookID'></div>");

            // Start conversion - fire and forget
            $command = "/usr/local/bin/curl \"$wgPrintBookRenderServerPath/request.php?id=$pageId&bookID=$bookID";
            $command .= "&wikiurl=" . $thisWikiDomain . "\" > /dev/null &";
            shell_exec($command);
            //open and handle progress bar
            $wgPrintBookRenderServerPath = preg_replace("/\//", "\/", $wgPrintBookRenderServerPath);
            //$out->addHTML($wgPrintBookRenderServerPath);
            $progressBar = "<div class='progress mb-2'>\n";
            $progressBar .= "<div id='printBookProgressBar' ";
            $progressBar .= "class='progress-bar progress-bar-striped progress-bar-animated' role='progressbar' aria-valuenow='0' aria-valuemin='0' aria-valuemax='100' style='width: 0%'>\n";
            $progressBar .= "</div>\n</div>\n";

            $buttons = "<a id='pbDownloadBtn' class='btn btn-primary d-none me-2 fa fa-download' role='button' style='color:white;text-decoration:none;' href='$wgPrintBookRenderServerPath/data/$thisWikiDomain/$pageId/$bookID.pdf' ";
            $buttons .= "download='Book.pdf'>" . $this->msg('printbook-getbook')->plain() . "</a>";
            $buttons .= "<button id='pbBackBtn' class='btn btn-secondary d-none role='link' type='button' data-href='$pageName'>";
            $buttons .= $this->msg('printbook-return')->plain();
            $buttons .= "</button>";

            $out->addHTML($progressBar . $messages . $buttons);
            $out->addModules('ext.PrintBook');
        }
    }
}

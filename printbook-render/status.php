<?php

/**
 * Get status of rendering book / article
 * status.php?id=articleId&wikiurl=wikiurl&bookid=bookID
 * @ingroup Extensions
 * @author Josef Martiňák
 */

// Get input
if(isset($_GET['id']) && !empty($_GET['id']) && isset($_GET['bookid']) && !empty($_GET['bookid'])) {
    $pageId = $_GET['id'];
    $bookId = $_GET['bookid'];
}
else {
    echo "error-missing-input";
    exit();
}

// Get content of progress file
if(isset($_REQUEST['wikiurl']) && !empty($_REQUEST['wikiurl']) && preg_match('/^[0-9a-zA-Z\-\.]+$/', $_REQUEST['wikiurl'])) {
    $wikiurl = $_REQUEST['wikiurl'];
    if($progress = file_get_contents(__DIR__ . "/data/$wikiurl/$pageId/$bookId.txt")) {
        echo rtrim($progress);
    }
    else {
        echo "error-no-progress-file";
        exit();
    }
}
else {
    echo "error-no-progress-file";
    exit();
}


?>
#!/usr/bin/python3
# Prepare PDF's of wiki articles
# Call "updateCache.py wikiUrl" for caching complete wiki
# Call "updateCache.py wikiUrl curid" for just one article
# @author Josef Martiňák

import os
import subprocess
import sys
import re
import shutil
import datetime
import pytz
import requests
import config

def writeProgress(savePath, text):
    with open(savePath + '/progress.txt', 'w') as progress:
        progress.write(text)

def logMsg(msg, type = 'error'):
    now = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    with open(config.appPath + '/log/' + type + '.log', 'a') as f:
        f.write(now + ': ' + msg + '\n')
        print(msg)

def cacheArticle(articlePath, articleId, wikiUrl, timestamp):
    try:
        print(articlePath)
        with open(articlePath + '/progress.txt', 'w') as progress: progress.write('0/1')
        out = subprocess.check_output('/usr/bin/wkhtmltopdf "' + 'https://' + wikiUrl + '/index.php?curid=' + str(articleId) + '&useskin=printer" ' + articlePath + '/tmp.pdf', shell=True)
        if re.match(r'(error|warning)', str(out), re.I) is None:
            shutil.move(savePath + '/tmp.pdf', savePath + '/Article.pdf')
            writeProgress(articlePath, 'finished' + '/' + str(timestamp))
            return True
        else:
            writeProgress(articlePath, 'error-article')
            logMsg(articlePath + ' :: ' + wikiUrl + ' :: error-article', type = 'error')
            return False
    except:
        logMsg('caching failed :: ' + wikiUrl + ' :: ' + str(articleId) + ' :: ' + str(sys.exc_info()[1]), type = 'error')
        print("caching failed")
        return False

def getArticleTS(pageid, wikiUrl):
    # get timestamp of current revision
    try:
        rUrl = 'https://' + wikiUrl + '/api.php?action=query&pageids=' + str(pageid) + '&format=json&prop=revisions&rvprop=timestamp'
        response = requests.get(rUrl)
        if response is not None:
            data = response.json()
            for pid, pd in data['query']['pages'].items():
                if pid == '-1': continue
                else:
                    for item in pd['revisions']:
                        return int( datetime.datetime.strptime(item['timestamp'], '%Y-%m-%dT%H:%M:%SZ').replace(tzinfo=pytz.utc).timestamp()  )
                        break
                    break
        else: return 0
    except:
        logMsg('get timestamp failed :: ' + wikiUrl + ' :: ' + str(pageid) + ' :: ' + str(sys.exc_info()[1]), type = 'error')

def deleteAllLockedCaches(dataPath):
    for root, dirs, files in os.walk(dataPath):
        if 'lock' in files:
            print(root)
            shutil.rmtree(root)



## START SCRIPT ##
wikiUrl = sys.argv[1]
if len(sys.argv) < 2:
    print("Wrong number of arguments !!")
    exit()


if wikiUrl not in config.wikiUrl:
    print('error-wrong-wikiurl')
    exit()

if len(sys.argv) > 2:
    pageId = sys.argv[2]
    pageTS = getArticleTS(sys.argv[2], wikiUrl)
    savePath = config.dataPath + '/' + wikiUrl + '/' + str(pageId)
    # create new cache without other checking
    try: os.mkdir(savePath)
    except: pass
    try: os.remove(savePath + '/tmp.pdf')
    except: pass
    with open(savePath + '/lock', 'w') as f: f.write('')
    if cacheArticle(savePath, pageId, wikiUrl, pageTS): os.remove(savePath + '/lock')
    else: logMsg('create cache failed :: ' + wikiUrl + ' :: ' + str(pageId), type = 'error')
else:
    # Get all articles in allowed namespaces
    for ns in config.allowedNS: 
        if ns[3] == 'global' or ns[3] == wikiUrl:   
            pageId = 0
            rUrl = 'https://' + wikiUrl + '/api.php?action=query&format=json&list=allpages&aplimit=500&apfilterredir=nonredirects&apnamespace=' + str(ns[0])
            response = requests.get(rUrl)
            apcontinue = True
            while apcontinue == True:
                if response is not None:
                    data = response.json()
                    for pg in data['query']['allpages']:

                        pageId = pg['pageid']
                        pageTS = getArticleTS(pg['pageid'], wikiUrl)

                        # check current status of cache files
                        savePath = config.dataPath + '/' + wikiUrl + '/' + str(pageId)
                        if os.path.exists(savePath + '/lock'):
                            with open(savePath + '/progress.txt', 'r') as file:
                                content = file.read()
                                if 'error' in content:
                                    shutil.rmtree(savePath)
                                    logMsg('locked cache folder with error deleted :: ' + wikiUrl + ' :: ' + str(pageId) + ' :: ' + content, type = 'error')
                                else:
                                    # do nothing, file in progress probably
                                    logMsg('locked file found :: ' + wikiUrl + ' :: ' + str(pageId) + ' :: ' + content, type = 'error')
                                    continue

                        # check timestamps
                        # Check if cache is valid
                        cacheTS = 0
                        if os.path.exists(savePath + '/progress.txt'):
                            with open(savePath + '/progress.txt') as f: progress = f.read()
                            match = re.search(r'^finished\/(.*)$', progress)
                            if match is not None: cacheTS = int(match.group(1))

                        if pageTS > cacheTS:
                            ## createCache
                            try: os.mkdir(savePath)
                            except: pass

                            # create lock file
                            with open(savePath + '/lock', 'w') as f: f.write('')
                            # init status file with progress 
                            writeProgress(savePath, 'initialising/initialising')
                            try: os.remove(savePath + '/tmp.pdf')
                            except: pass

                            if cacheArticle(savePath, pageId, wikiUrl, pageTS):
                                # unlock
                                try:
                                    os.remove(savePath + '/lock')
                                except: pass
                    if "continue" in data:
                        if "apcontinue" in data['continue']:
                            print(data['continue']['apcontinue'])
                            rUrl += "&apcontinue=" + data['continue']['apcontinue']
                            response = requests.get(rUrl)
                        else: apcontinue = False
                    else: apcontinue = False
                else: apcontinue = False

# set rights
subprocess.check_output('chown -R www-data:www-data ' + config.dataPath + '/' + wikiUrl, shell=True)
subprocess.check_output('chmod -R ugo=rwx ' + config.dataPath + '/' + wikiUrl, shell=True)

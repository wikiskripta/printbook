<?php

/**
 * Request PDF of book / article
 * request.php?id=articleId&bookID=random_string&wikiUrl=wikiurl
 * Progress of rendering and PDF to download can be found at status.php
 * If bookID not set update pdf of an article
 * @ingroup Extensions
 * @author Josef Martiňák
 */


// Get input
$pageId = $bookID = $wikiurl = '';
if(isset($_REQUEST['id']) && !empty($_REQUEST['id']) && preg_match('/^[0-9]+$/', $_REQUEST['id'])) $pageId = $_REQUEST['id'];
if(isset($_REQUEST['wikiurl']) && !empty($_REQUEST['wikiurl']) && preg_match('/^[0-9a-zA-Z\-\.]+$/', $_REQUEST['wikiurl'])) $wikiurl = $_REQUEST['wikiurl'];
if(isset($_REQUEST['bookID'])) $bookID = $_REQUEST['bookID'];

if(empty($pageId) || empty($wikiurl)) {
    echo "error-missing-input";
    exit();
}

if(empty($bookID)) {
    // update article's pdf
    $command = escapeshellcmd("/usr/bin/python " .__DIR__ . "/updateCache.py $wikiurl $pageId > /dev/null &");
}
else {
    // create book
    $command = escapeshellcmd("/usr/bin/python " .__DIR__ . "/createBook.py $pageId $bookID $wikiurl > /dev/null &");
}
// Start rendering
ignore_user_abort(true);
shell_exec($command);

?>

#!/usr/bin/python3
# Prepare PDF of wiki book
# Article name in argument (createBook.py articleId bookId wikiUrl)
# @author Josef Martiňák

import os
import subprocess
import sys
import re
import time
import datetime
import json
import requests
import config

def writeProgress(savePath, text):
    with open(savePath, 'w') as progress:
        progress.write(text)

def logMsg(msg, type = 'error'):
    now = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    with open(config.appPath + '/log/' + type + '.log', 'a') as f:
        f.write(now + ': ' + msg + '\n')
        print(msg)

def check_article_exists(wiki_url, article_title):
    params = {
        'action': 'query',
        'titles': article_title,
        'format': 'json'
    }
    response = requests.get('https://' + wiki_url + '/api.php', params=params)
    data = json.loads(response.text)

    if 'error' in data:
        # Handle API errors
        return False

    pages = data['query']['pages']
    for page in pages.values():
        if 'missing' in page:
            return False  # Article doesn't exist
        else:
            return True  # Article exists

articleId = sys.argv[1]
bookId = sys.argv[2]
wikiUrl = sys.argv[3]
savePath = config.dataPath + '/' + wikiUrl + '/' + articleId
userEmail = ''

if len(sys.argv) < 4:
    print("Wrong number of arguments !!")
    exit()

if wikiUrl not in config.wikiUrl:
    print(savePath, 'error-wrong-wikiurl')
    exit()

try:
    if os.path.exists(savePath) == False:
        logMsg(savePath + ' :: ' + wikiUrl + ' :: error-book', type = 'error')
        writeProgress(savePath, 'book-error :: path not exists')
        quit()

    # Remove old versions
    cutoff_time = time.time() - 2 * 3600  # Convert hours to seconds
    for root, dirs, files in os.walk(savePath):
        for file in files:
            file_path = os.path.join(root, file)
            file_mtime = os.path.getmtime(file_path)
            if file_mtime < cutoff_time:
                if file in ['Article.pdf', 'progress.txt', 'lock']: continue
                os.remove(file_path)

    # init status file with progress 
    progressPath = savePath + '/' + bookId + '.txt'
    writeProgress(progressPath, 'initialising/initialising')

    rUrl = 'https://' + wikiUrl + '/api.php?action=parse&pageid=' + articleId + '&prop=wikitext&formatversion=2&format=json&redirects'
    response = requests.get(rUrl, timeout = 15)
    if response is None:
        logMsg(progressPath + ' :: error-book', type = 'error')
        writeProgress(progressPath, 'error-book-request')
        exit()
    else:
        data = response.json()
        wikitext = data['parse']['wikitext']
        booktitle = data['parse']['title']
        chapterList = []
        chapterCounter = 0
        chapterCount = ''
        # get all articles in the book
        for chapter in re.findall(r'\[\[(.*?)\]\]', wikitext):
            chapter = re.sub(r' *\|.*', '', chapter)
            chapter = re.sub(r'#.*', '', chapter)
            # skip some namespaces
            allowedNamespace = False
            if ':' not in chapter:
                # main NS always allowed
                allowedNamespace = True
            else:
                for ns in config.allowedNS: 
                    if ns[0] == 0: continue
                    elif ((ns[1] + ':') in chapter or (ns[2] + ':') in chapter) and (ns[3] == 'global' or ns[3] == wikiUrl): 
                        allowedNamespace = True
                        break
            if allowedNamespace == False: continue

            # skip non-existent chapters
            if check_article_exists(wikiUrl, chapter) == False: continue

            # get chapter id
            rUrl = 'https://' + wikiUrl + '/api.php?action=query&titles=' + chapter + '&format=json&prop=revisions&rvprop=timestamp&redirects'
            response = requests.get(rUrl, timeout = 15)
            if response is not None:
                data = response.json()
                for pid, pd in data['query']['pages'].items():
                    if pid == '-1': continue
                    else:
                        chapterCounter += 1
                        chapterList.append(pid)
                        break
            # update progress file
            chapterCount = str(chapterCounter)
            writeProgress(progressPath, '0/' + chapterCount)

        if chapterCounter == 0:
            writeProgress(progressPath, 'error-empty-book')
            logMsg(progressPath + ' :: error-empty-book', type = 'error')
            exit()

        chapterCounter = 0
        pdftkCmd = '/usr/bin/pdftk'
        pdftkCmd += ' ' + savePath + '/Article.pdf' # TOC
        for chapter in chapterList:
            # create pdf join command
            chapterCounter += 1
            chapterSavePath = config.dataPath + '/' + wikiUrl + '/' + str(chapter)

            # skip if cache not exists
            if os.path.exists(chapterSavePath) == False:
                logMsg(chapterSavePath + ' :: ' + chapterSavePath + ' :: error-book-excluded :: cache missing', type = 'error')
                continue

            # handle locked files
            limit = 0
            while os.path.exists(chapterSavePath + '/lock'):
                if limit < 20:
                    time.sleep(0.1)
                    limit = limit + 1
                else:
                    if os.path.exists(chapterSavePath + '/Article.pdf'):
                        logMsg(progressPath + ' :: error-book :: locked. Prev version used', type = 'error')
                    else:
                        # stranka bude chybět v knize
                        if os.path.exists(chapterSavePath + '/lock'): os.remove(chapterSavePath + '/lock')
                        if os.path.exists(chapterSavePath + '/tmp.pdf'): os.remove(chapterSavePath + '/tmp.pdf')
                        logMsg(progressPath + ' :: ' + chapterSavePath + ' :: error-book-excluded :: locked. Lock file removed', type = 'error')
                    break

            if limit == 20:
                # stranka bude chybět v knize
                if os.path.exists(chapterSavePath + '/lock'): os.remove(chapterSavePath + '/lock')
                logMsg(progressPath + ' :: ' + chapterSavePath + ' :: error-book-excluded :: locked. Lock file removed', type = 'error')
                continue

            pdftkCmd += ' ' + chapterSavePath + '/Article.pdf'
            writeProgress(progressPath, str(chapterCounter) + '/' + chapterCount)

        # join pdf
        pdftkCmd += " cat output " + savePath + '/' + bookId + '.pdf'
        #print(pdftkCmd)
        out = subprocess.check_output(pdftkCmd, shell=True)
        if re.match(r'(error|warning)', str(out), re.I) is None:
            # save finish state
            writeProgress(progressPath, 'finished' + '/?')
        else:
            #print("Book error (pdftk): " + articleId)
            writeProgress(progressPath, 'error-pdftk')
            logMsg(progressPath + ' :: error-book :: pdftk failed', type = 'error')
            exit()
except:
    logMsg('book error :: ' + savePath + ' :: ' + wikiUrl + ' :: ' + str(sys.exc_info()[1]), type = 'error')
    #writeProgress(savePath, 'error: ' + str(sys.exc_info()[1]))


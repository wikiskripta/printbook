#!/bin/sh

# clear all locked files
cd /var/www/html/printbook-render/data

chown -R www-data:www-data *
chmod -R ugo=rwx *

rm */1*/lock
rm */2*/lock
rm */3*/lock
rm */4*/lock
rm */5*/lock
rm */6*/lock
rm */7*/lock
rm */8*/lock
rm */9*/lock


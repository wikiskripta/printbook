( function ( mw, $ ) {

	var pageId = $("#pbConfig").data('pageid');
	var bookID = $("#pbConfig").data('bookid');
	var renderServerPath = $("#pbConfig").data('renderpath');

	var timer;
	function startTimer() {
		timer = setTimeout(function(){ checkStatus(); }, 2000);
	}

	function fileExists(pageId, filename) {
		$.ajax({
			url: renderServerPath + '/data/' + location.hostname + '/' + pageId + '/' + filename,
			type: 'HEAD',
			success: function(data) {
				console.log('Success:', data);
				 return true;
			},
			error: function() {
				console.log('Error');
				return false;
			}
		});
	}

	function checkStatus() {
		clearTimeout(timer);
		//console.log(renderServerPath + '/status.php?id=' + pageId + '&bookid=' + bookID + '&wikiurl=' + location.hostname);
		$.ajax({
			type: 'GET',
			url: renderServerPath + '/status.php?id=' + pageId + '&bookid=' + bookID + '&wikiurl=' + location.hostname,
			success: function( server_response ) {
				if(server_response.includes('error')) {
					// rendering failed
					/*
					if(fileExists(pageId, bookID + '.pdf')) {
						$("#printBookProgressBar").addClass("d-none");
						$("#pbInfo").addClass("d-none");
						$("#pbDownloadBtn").removeClass("d-none");
						return true;
					}
					*/
					$("#pbBackBtn").removeClass("d-none");
					$("#printBookError").removeClass("d-none");
				}
				else {
					// update status
					if(server_response.includes('finished')) {
						// show download and back button
						$("#printBookProgressBar").attr('aria-valuenow', '100');
						$("#printBookProgressBar").css('width', '100%');
						$("#pbInfo").addClass("d-none");
						$("#printBookSuccess").removeClass("d-none");
						$("#pbDownloadBtn").removeClass("d-none");
						$("#pbBackBtn").removeClass("d-none");
					}
					else if(server_response.includes('initialising')) {
						$("#printBookProgressBar").attr('aria-valuenow', '1');
						$("#printBookProgressBar").css('width', '1%');
						startTimer();
					}
					else {
						// get progress bar value and update
						var res = server_response.trim().split("/");
						var pbCount = parseInt(res[0]);
						var pbMax = parseInt(res[1]);
						var progressBarValue = Math.round(100*pbCount/pbMax);
						$("#printBookProgressBar").attr('aria-valuenow', progressBarValue);
						$("#printBookProgressBar").css('width', progressBarValue + '%');
						startTimer();
					}
				}
			}
		});
	}

	// handle buttons
	$( "#pbBackBtn" ).click(function() {
		location.href = location.origin + '/w/' + $(this).data('href');
	});

	// start updating progress bar
	startTimer();

}( mediaWiki, jQuery ) );
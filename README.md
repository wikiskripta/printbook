# PrintBook

Mediawiki extension. PDF version of a book or article.

## Description

* Version 4.0
* Extension prepares print version of an article and creates books.
* Init py script caches, all articles at the start.
* When article changes (or new one is created), a mw hook creates new cache.
* Cache is deleted when article deleted.
* No need to check validity.
* No book caching - just join existing articles' caches.
* Book chapters in transcluded articles are ignored.

## Installation

### Wiki side

* Make sure you have MediaWiki 1.39+ installed.
* Bootstrap 5 required.
* Skin [Printer](https://bitbucket.org/wikiskripta/printer) installed for articles' print versions.
* Download and place the extension to your /extensions/ folder.
* Add the following code to your LocalSettings.php:

``` php
wfLoadExtension( 'PrintBook' );
$wgPrintBookRenderServerPath = network path to "printbook-render" folder;
```

### Rendering server side

* Install Debian packages _wkhtmltopdf_, _pdftk_ and Python 3.
* Place _printbook-render_ folder to www folder.
* Copy _config.py.sample_ to _config.py_ and set variables.
* _printbook-render/data_ folder public.
* Run _updateCache.py_ that caches all articles.
* Allow CORS for domain and change pdf's mime to download instead of displaying

``` shell
# Nginx example
add_header "Access-Control-Allow-Origin" https://www.wikiskripta.eu always;
Note: pridej do volane adresy na webservcices url volajiciho a podle toho nsatav nginx cors
add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';
add_header 'Access-Control-Allow-Headers' 'DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range';
add_header 'Access-Control-Expose-Headers' 'Content-Length,Content-Range';

location ~ ^/printbook-render/data/.*\.pdf$ {
    types {
        application/octet-stream pdf;
    }
}
```

## Internationalization

This extension is available in English and Czech language. For other languages, just edit files in /i18n/ folder.

## Release Notes

### 2.1

* Caching
* Send email with download link when finished
* Allow multiple wikis

### 2.2

* Querystring parameter "printable=yes" is deprecated. 
* Using new SpecialPage feature: _Special:PrintBook/pagename/printable_ returns print version of a wiki page.
* New file _resources\ext.PrintBookPrintable.css_ for preparing print input.

### 2.2.1

* _printable=yes_ parameter offering the article's print version on MW is deprecated.
* Instead we use _useskin=printer_ of the [Printer skin](https://bitbucket.org/wikiskripta/printer).

### 3.0 DEV - books not working

* Separate data folder for each site
* Called webservices URL contains id of caller
* Every article has its cache (foldername=curid) updated if not valid before serving
* Book updates articles' caches that are not valid and create a new book cache if necessary
* Old caches are not deleted before caching. They are replaced with new version when old cache file is writable
* Cache folder contains info if in progess. In this case, old cache file is served. Or wait until new file is created.

### 4.0 

* Previous versions didn't work well.
* Init job caches all articles.
* When article changes, a mw hook caches an article again.
* No need to check validity.
* No book caching - just join existing articles' caches.

## Authors and license

* [Josef Martiňák](https://www.wikiskripta.eu/w/User:Josmart), [Petr Kajzar](https://www.wikiskripta.eu/w/User:Slepi)
* MIT License, Copyright (c) 2024 First Faculty of Medicine, Charles University
